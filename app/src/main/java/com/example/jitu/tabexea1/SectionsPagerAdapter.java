package com.example.jitu.tabexea1;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;



/**
 * Created by jitu on 2/4/2017.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter{

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }



    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new India();
            case 1:
                return new Entertainment();
            case 2:
                return new Sports();
            case 3:
                return new Technology();
            case 4:
                return new World();
            case 5:
                return new Cricket();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "India";
            case 1:
                return "Entertainment";
            case 2:
                return "Sports";
            case 3:
                return "Technoloy";
            case 4:
                return "World";
            case 5:
                return "Cricket";
        }
        return null;
    }


}