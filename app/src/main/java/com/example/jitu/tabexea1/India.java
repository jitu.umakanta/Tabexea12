package com.example.jitu.tabexea1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;


public class India extends Fragment {

    RecyclerView recyclerView;
    //private final static String API_KEY = "2";
    RecyclerView.Adapter adapter;
    int requestCount = 1;
    List<news> listArticles;
    List<news> listTrndingNewsData9;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listArticles = new ArrayList<>();
        listTrndingNewsData9 = new ArrayList<>();

    }
    public India() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_india, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // retrocall();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //requestCount = 1;
       /* adapter = new CardAdapter1(listTrndingNewsData9, getActivity());
        recyclerView.setAdapter(adapter);*/
        getData();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (isLastItemDisplaying(recyclerView)) {

                    getData();

                }
            }
        });

    }

    private void getData() {

        retrocall(requestCount);
        //Incrementing the request counter
        requestCount++;
    }

    public void retrocall(int API_KEY) {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<newsresponse> call = apiService.getTopRatedMovies(API_KEY);
        call.enqueue(new Callback<newsresponse>() {
            @Override
            public void onResponse(Call<newsresponse> call, Response<newsresponse> response) {
                int statusCode = response.code();
                listArticles = response.body().getArticles();
                //recyclerView.setAdapter(new RecycleViewAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                System.out.println(listArticles);
                adapter = new CardAdapter1(listArticles, getActivity());
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<newsresponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });


    }
    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("hhhhhh", "ggggggggggggggggg");
    }
}
